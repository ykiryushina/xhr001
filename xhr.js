function login() {
    let json = JSON.stringify({
        "username": "george.bluth@reqres.in",
        "email": "george.bluth@reqres.in",
        "password": "george"
    });
    let xhr = new XMLHttpRequest();
    xhr.open('POST', 'https://reqres.in/api/login', false);
    xhr.setRequestHeader('Content-Type', 'application/json;charset=utf-8');
    xhr.send(json);
    let response = xhr.response;
    let token = response.substring(10, 27);
    getUsers(token);
}
window.onload = login;

function getUsers(token) {
    let xhr = new XMLHttpRequest();
    let json = JSON.stringify({
        "page": 1,
        "per_page": 10,
        "total": 10,
        "total_pages": 0,
        "data": [
            {
                "id": 0,
                "email": "string",
                "first_name": "string",
                "last_name": "string",
                "avatar": "string"
            }
        ]
    })
    xhr.open('GET', 'https://reqres.in/api/users?page=1&per_page=10');
    xhr.setRequestHeader('Cache-Control', 'no-cache');
    xhr.setRequestHeader('Accept-Language', 'ru-RU,ru');
    xhr.setRequestHeader('Content-Type', 'application/json;charset=utf-8');
    xhr.setRequestHeader('Authorization', token);
    xhr.send(json);
    xhr.onloadend = () => {
        function getEmails() {
            let usersArr = JSON.parse(xhr.response).data;
            let emails = [];
            for (let i = 0; i < usersArr.length; i++) {
                emails.push(usersArr[i].email);
            }
            console.log(emails);
        }
        getEmails();
    };

    xhr.onreadystatechange = () => {
        console.log(xhr.readyState);
    }
}